/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/typedef */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable prefer-rest-params */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable max-classes-per-file */

export type attrs = Record<string, string>;
export const defaultRelayAttrName = (webcomponent: any) =>
  `relaying-host-${kebabize(webcomponent.constructor.name)}-attrs`;
export const defaultRelayAttr = (webcomponent: any) => ({
  [defaultRelayAttrName(webcomponent)]: `unique-key:${kebabize(
    webcomponent.constructor.name
  )}`,
});

/**
 * Transforms a camelCase string to kebab-case
 * @example
 * kebabize('yourString') // returns 'your-string'
 */
export const kebabize = (str: string) =>
  str.replace(
    /[A-Z]+(?![a-z])|[A-Z]/g,
    ($, ofs) => (ofs ? "-" : "") + $.toLowerCase()
  );

/**
 * Transforms a kebab-case string to camelCase
 * @example
 * camelize('your-string') // returns 'yourString'
 */
export const camelize = (str: string) =>
  str.replace(/-./g, (x) => x[1].toUpperCase());

/**
 * Update the inner element with the added attributes on the host element.
 * If the attributes are not already added to the host component as props, they will be copied to the target inner element.
 *
 * ## Usage
 * ```html
 * <!-- If the pwc-component only has a prop 'required' -->
 * <pwc-component required="console.log(`This is NOT relayed as it's a host prop`)"
 *                no-host-prop="console.log('This is relayed')">
 * </pwc-component>
 * ```
 * ### Options
 * - We can force attributes to be relayed by using `relayer:force-relay='["^regex","^another"]'` - in this case, any attr that starts with 'regex' or 'another' is force-copied.
 * - We can prevent attributes to be relayed by using `relayer:dont-relay='["^regex"]'` - in this case, any attr that starts with 'regex' is NOT copied. The force-option trumps this option.
 *
 * ### AlpineJS support
 * We added full support for this [awesome mini-framework](https://github.com/alpinejs/alpine).
 * The intention is that it should work as you expect.
 * - `x-bind:attribute` will relay only if said attribute is not added as a host prop;
 * - `x-model` will always relay;
 * - `x-on:event` and `@event` will always relay;
 * - Any other AlpineJS attribute like x-show, x-cloak will not be relayed.
 *
 * Bear in mind that you can always overwrite these default settings using the options mentioned above.
 *
 * ## Setup in Stencil project
 * Requires `@Element() el` to have been added.
 * @example
 * // The semi-colon before the decorators is needed to render the example correctly
 * ;@Element() el: HTMLElement;
 * ;@RelayAttrs() relayedAttrs;
 *
 * render() {
 *     <input {...relayedAttrs} />
 * }
 */
export function RelayAttrs(): PropertyDecorator {
  // tslint:disable-next-line: only-arrow-functions - we need it to be a regular function to preserve `this`
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return function (target: any, name?: string): void {
    const descriptor: PropertyDescriptor = {
      value: defaultRelayAttr(target),
      writable: false,
    };
    Object.defineProperty(target, name, descriptor);

    const origComponentDidLoad = target.componentDidLoad;
    target.componentDidLoad = function () {
      this.relayer = new Relayer(this);

      // And call the original
      if (typeof origComponentDidLoad === "function")
        origComponentDidLoad.apply(this, arguments);
    };

    const origDisconnectedCallback = target.disconnectedCallback;
    target.disconnectedCallback = function () {
      (this.relayer as Relayer).onDestroy();

      // And call the original
      if (typeof origDisconnectedCallback === "function")
        origDisconnectedCallback.apply(this, arguments);
    };
  };
}

export class Relayer {
  mutationObserver: MutationObserver;
  hostElement: HTMLElement;
  relayingAttr: string;
  selector: string;
  legacyRelayAttrPrefix = "relay-";
  relayOptionPrefix: string = "relayer:";
  relayTargetElements: NodeListOf<Element>;

  private _dontRelay: Array<RegExp> = [];
  private _forceRelay: Array<RegExp> = [];
  // eslint-disable-next-line @typescript-eslint/typedef
  relayOptions = {
    selector: (val: string) => (this.selector = val),
    dontRelay: (val: string) =>
      (this._dontRelay = this.parseStrToArrOfRegex(val)),
    forceRelay: (val: string) =>
      (this._forceRelay = this.parseStrToArrOfRegex(val)),
  };

  constructor(private originalWebComponent: any) {
    this.hostElement = this.originalWebComponent.el;

    // If we're just mocking the relayer, simply don't do anything
    if (
      typeof this.hostElement === "string" &&
      (this.hostElement as string).includes("mock")
    ) {
      return;
    }

    this.relayingAttr = defaultRelayAttrName(this.originalWebComponent);
    this.selector = `[${this.relayingAttr}]`;

    if (!this.hostElement) {
      throw new ErrorWithArgs(
        'Please also add `@Element() el: HTMLElement` to your Stencil component (`import { Element } from "@stencil/core"`)',
        this
      );
    }

    this.init();
  }

  public init() {
    let allHostElAttrs = Object.values(this.hostElement.attributes);
    if (globalThis.isRunningTests) {
      // Stencil's default mock of the attributes isn't implemented correctly, so we have to overwrite it
      // Extracting it to a separate function to mock won't work as the current method is instantiated on load
      allHostElAttrs = (allHostElAttrs[1] as unknown as Array<any>)?.map(
        (faultilyMockedAttr: any) => {
          return {
            name: faultilyMockedAttr._name,
            value: faultilyMockedAttr._value,
          };
        }
      ) as Array<Attr>;
    }

    const attributesWithoutOptions = allHostElAttrs.filter((attr: Attr) => {
      const key = attr.name;

      if (key.startsWith(this.relayOptionPrefix)) {
        const optionAttr = key.replace(this.relayOptionPrefix, "");
        const option = this.relayOptions[camelize(optionAttr)];
        if (!option) {
          throw new ErrorWithArgs(
            `There's no option called '${optionAttr}'.
                    These are all the available options: ${Object.keys(
                      this.relayOptions
                    )
                      .map(kebabize)
                      .join(", ")}`,
            this.hostElement
          );
        }
        option(attr.value);

        // If it's an option, we don't want to relay this attr
        return false;
      }

      return true;
    });

    this.relayTargetElements = this.getTargetElements();
    if (!this.relayTargetElements.length) {
      throw new ErrorWithArgs(
        `No target elements found with selector '${this.selector}'.`
      );
    }

    for (const attr of attributesWithoutOptions) {
      this.relayAttrIfItShould(attr.name);
    }

    this.addListeners();
  }

  onAttributesChange(changedAttrs: Array<string>) {
    for (const changedAttr of changedAttrs) {
      this.relayAttrIfItShould(changedAttr);
    }
  }

  relayAttrIfItShould(changedAttr: string) {
    if (!this.checkShouldRelayAttr(changedAttr)) return;

    const hostHasAttr = this.hostElement.hasAttribute(changedAttr);
    if (!hostHasAttr) return;

    const hostAttrValue = this.hostElement.getAttribute(changedAttr);

    // If it should be relayed, just do something with it if it's not already removed. Prevent infinite loop
    if (this.checkShouldRelayAttr(changedAttr)) {
      this.addAttrToTargetElements(changedAttr, hostAttrValue);
      this.hostElement.removeAttribute(changedAttr);
    }
  }

  addListeners() {
    // ! Be very careful refactoring this line, as Jest doesn't work with MutationObservers. So we couldn't test this line
    this.mutationObserver = new MutationObserver((mutations) =>
      this.onAttributesChange(mutations.map((m) => m.attributeName))
    );
    this.mutationObserver.observe(this.hostElement, {
      attributes: true,

      // and disable the rest of the mutations-listeners
      childList: false,
      subtree: false,
      characterData: false,
      characterDataOldValue: false,
      attributeOldValue: false,
    });
  }

  onDestroy() {
    this.mutationObserver?.disconnect();
  }

  /**
   * Parses a string to an array of regular expressions
   * @example
   * parseStrToArrOfRegex(`["test", "\s"]`); // returns [/test/, /\s/];
   */
  private parseStrToArrOfRegex(str: string): Array<RegExp> {
    let parsedVal: any;
    try {
      parsedVal = JSON.parse(str);
    } catch (e) {
      throw new ErrorWithArgs(
        `This is no valid JSON: ${"`" + str + "`"}.
                Make sure to use double quotes around everything.
                Example input: ${
                  this.relayOptionPrefix
                }:your-option='${'`["s","foo"]`'}'`,
        str,
        this.hostElement
      );
    }
    return (this._dontRelay = (
      Array.isArray(parsedVal) ? parsedVal : [parsedVal]
    ).map((v) => new RegExp(v)));
  }

  /**
   * Does the same as Array.includes, but instead checks against a regex
   * @example
   * includesAsRegex([/test/], 'testtest'); // returns true
   * includesAsRegex([/\s/], 'testtest'); // returns false
   */
  private includesAsRegex = (arr: Array<RegExp>, attr: string): boolean => {
    // Return as soon as one match is found, saves resources
    for (const regex of arr) {
      if (regex.test(attr)) return true;
    }
    return false;
  };

  /**
   * Check if we should relay the attr
   */
  private checkShouldRelayAttr(attr: string) {
    attr = attr.replace(this.legacyRelayAttrPrefix, ""); // Offer backward compatibility

    // Always relay if it's forced
    if (this.includesAsRegex(this._forceRelay, attr)) return true;

    const neverRelayThese = [
      "title",
      "style",
      "class",
      "data-hmr",
      "data-testid",
    ];
    if (neverRelayThese.includes(attr)) return false;

    // Never relay if it's excluded and not forced
    if (this.includesAsRegex(this._dontRelay, attr)) return false;

    // This checks if the host component has a prop with the same name
    const allHostProps: string[] = [];
    // tslint:disable-next-line: forin - makes no sense here because we need ALL props
    for (const prop in this.originalWebComponent) {
      allHostProps.push(prop);
    }

    const hostHasProp = allHostProps.filter((prop: string) => {
      // First kebabize the prop, so hostPropValue >> host-prop-value
      const kebabizedProp = kebabize(prop);
      if (attr === kebabizedProp) {
        return true;
      }

      // AlpineJS support https://github.com/alpinejs/alpine
      if (attr.startsWith(`x-`) || attr.startsWith("@")) {
        // Count as host prop when x-bind refers a host prop
        // So if 'value' is a host prop, also keep 'x-bind:value' on the host
        if (attr.startsWith("x-bind:")) {
          return attr.includes(`x-bind:${kebabizedProp}`);
        }

        // Don't count x-on and @ (which is an alternate way of writing x-on). For blurring, input and overall predictability.
        // That latter is why we don't distinguish between click/hover and other events: we wan't it to be predictable.
        // They can always use the dont-relay option to change that behaviour
        if (attr.startsWith(`x-on`) || attr.startsWith("@")) return false;

        // Don't count x-model as host prop, because that should go directly on the input
        if (attr.startsWith(`x-model`)) return false;

        // And count as host prop if it's any other Alpine function like x-show, x-cloak and so on
        return true;
      }

      return false;
    }).length;

    return !hostHasProp;
  }

  private getTargetElements() {
    const targetElements: NodeListOf<Element> = (
      this.hostElement.shadowRoot
        ? this.hostElement.shadowRoot
        : this.hostElement
    ).querySelectorAll(this.selector);

    return targetElements;
  }

  private addAttrToTargetElements(attribute: string, value: string) {
    this.relayTargetElements?.forEach((el) => {
      el.setAttribute(attribute.replace(this.legacyRelayAttrPrefix, ""), value);
    });
  }
}
export class MockRelayer extends Relayer {
  constructor(el?: HTMLElement) {
    super({
      el:
        el ||
        `mocking the relayer, don't need the functionality, just the methods and props`,
    });
  }
}

class ErrorWithArgs extends Error {
  constructor(errMessage: string, ...extraArgs: any[]) {
    super(errMessage);
    // Add the extra args after the normal error
    setTimeout(() => extraArgs.forEach((arg) => console.error(arg)));
  }
}
