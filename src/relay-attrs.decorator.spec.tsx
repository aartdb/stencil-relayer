import { Component, Element, h } from '@stencil/core';
import { Prop } from '@stencil/core/internal';
import { newSpecPage, SpecPage } from '@stencil/core/testing';

import { attrs, kebabize, MockRelayer, RelayAttrs, Relayer } from './relay-attrs.decorator';

describe('RelayAttrs', () => {
  it('should build', () => {
    expect(new TestingRelay()).toBeTruthy();
  });

  describe('rendering', () => {
    let page: SpecPage;
    let defaultRelayer: Relayer;
    let relayer: Relayer;
    let relayTarget: HTMLElement;
    let addTheseAttrs: testAttrs;

    beforeAll(() => {
      defaultRelayer = new MockRelayer();
    });

    beforeEach(() => {
      addTheseAttrs = [
        {
          name: 'default-added-attr',
          value: 'default-added-value',
        },
      ];
    });

    const exec = async () => {
      page = await newSpecPage({
        components: [TestingRelay],
        // Add the attrs (that were probably changed in the `it`-functions) as HTML attributes
        html: `<testing-relay ${addTestAttrs(addTheseAttrs)}></testing-relay>`,
      });
      await page.waitForChanges();
      relayer = page.rootInstance.relayer;
      relayTarget = page.doc.querySelector(`[data-testid="relay-target"]`);
    };

    describe(`rendering`, () => {
      it('should be able to render component', async () => {
        await exec();

        expect(page.root).toBeTruthy();
      });
    });

    describe(`relaying`, () => {
      const addedHostProp = 'thisOneIsAddedToTheHostProp';
      const kebabizedAddedHostProp = 'this-one-is-added-to-the-host-prop';

      describe(`happy flow`, () => {
        it('should have a functioning Relayer attached', async () => {
          await exec();

          expect(relayer).toBeTruthy();
        });

        it(`should have set the relay attribute on the input`, async () => {
          await exec();

          expect(relayTarget).toBeTruthy();
          // And check if it's got the relay attr added
          expect(relayTarget).toHaveAttribute(relayer.relayingAttr);
        });

        it(`should still work with the old 'relay-' prefix`, async () => {
          const cleanAttr = `attr-${Math.random()}`;
          const fakeAttr = {
            name: `${relayer.legacyRelayAttrPrefix}${cleanAttr}`,
            value: `value ${Math.random()}`,
          };
          addTheseAttrs.push(fakeAttr);

          await exec();

          expect(relayTarget).toHaveAttribute(cleanAttr);
        });

        describe(`one load versus on change`, () => {
          const theMethodThatShouldBeCalled = jest.spyOn(Relayer.prototype, 'relayAttrIfItShould');
          it(`should call the same relay method on LOAD`, async () => {
            console.warn = jest.fn(); // We don't need the warning that it changed during rendering

            const addAttr = {
              name: `add-me-${Math.random()}`,
            };
            addTheseAttrs.push(addAttr);

            await exec();

            expect(theMethodThatShouldBeCalled).toHaveBeenCalledWith(addAttr.name);
          });

          it(`should call the same relay method on CHANGE`, async () => {
            const dynAddAttr = `add-me-dynamically-${Math.random()}`;

            await exec();

            page.root.setAttribute(dynAddAttr, '');
            // ! Because the MutationObserver isn't working, we need to call the method directly...
            relayer.onAttributesChange([dynAddAttr]);

            expect(theMethodThatShouldBeCalled).toHaveBeenCalledWith(dynAddAttr);
          });
        });

        describe(`adding relay attr which is already added as a prop in the component host`, () => {
          it(`should not relay the attr if it's exactly the same`, async () => {
            const hostProp = 'nocamelcasehostprop';
            const fakeAttr = {
              name: `${hostProp}`,
              value: 'testvalue',
            };
            addTheseAttrs.push(fakeAttr);

            await exec();

            expect(relayTarget).not.toHaveAttribute(fakeAttr.name.toLowerCase());
          });

          it(`should not relay the attr if it's kebabized the same`, async () => {
            // Just to make sure: the text should be the same
            expect(kebabizedAddedHostProp.replace(/-/g, '')).toEqual(addedHostProp.toLowerCase());

            const fakeAttr = {
              name: `${kebabizedAddedHostProp}`,
              value: 'testvalue',
            };
            addTheseAttrs.push(fakeAttr);

            await exec();

            expect(relayTarget).not.toHaveAttribute(kebabizedAddedHostProp);
          });
        });

        describe(`alpine js support`, () => {
          it(`should relay x-on`, async () => {
            const xOnClick = {
              name: `x-on:click`,
              value: `console.log('clicked')`,
            };

            // @something is the same as x-on:something with alpine
            const atBlur = {
              name: `@blur`,
              value: `console.log('blurred')`,
            };
            addTheseAttrs.push(xOnClick, atBlur);

            await exec();

            expect(relayTarget.getAttribute(xOnClick.name)).toEqual(xOnClick.value);
            expect(relayTarget.getAttribute(atBlur.name)).toEqual(atBlur.value);
          });

          it(`should relay x-model`, async () => {
            const xModel = {
              name: `x-model`,
              value: `theJsVarToModel`,
            };
            addTheseAttrs.push(xModel);

            await exec();

            expect(relayTarget.getAttribute(xModel.name)).toEqual(xModel.value);
          });

          it(`should NOT relay x-cloak, x-show and any other`, async () => {
            const xCloak = {
              name: `x-cloak`,
              value: `theJsVar`,
            };
            const xShow = {
              name: `x-show`,
              value: `theJsVar`,
            };
            const anyOther = {
              name: `x-show`,
              value: `theJsVar`,
            };
            addTheseAttrs.push(xCloak, xShow, anyOther);

            await exec();

            expect(relayTarget).not.toHaveAttribute(xCloak.name);
            expect(relayTarget).not.toHaveAttribute(xShow.name);
            expect(relayTarget).not.toHaveAttribute(anyOther.name);
          });

          describe(`x-bind`, () => {
            it(`should relay if it's NOT added as a host prop`, async () => {
              const xBind = {
                name: `x-bind:not-added-${Math.random()}`,
                value: `theJsVar`,
              };
              addTheseAttrs.push(xBind);

              await exec();

              expect(relayTarget.getAttribute(xBind.name)).toEqual(xBind.value);
            });

            it(`should NOT relay if it's added as a host prop`, async () => {
              const xBind = {
                name: `x-bind:${kebabizedAddedHostProp}`,
                value: `theJsVar`,
              };
              addTheseAttrs.push(xBind);

              await exec();

              expect(relayTarget).not.toHaveAttribute(xBind.name);
            });
          });
        });

        describe(`options`, () => {
          describe(`selector option`, () => {
            it(`should change the selector if the option is passed`, async () => {
              const customSelector = '[for-custom-selector]';
              const addThisAttr = {
                name: `test-default`,
                value: 'testvalue',
              };
              const fakeAttrs = [
                { ...addThisAttr },
                {
                  name: `${defaultRelayer.relayOptionPrefix}${defaultRelayer.relayOptions.selector.name}`,
                  value: customSelector,
                },
              ];
              addTheseAttrs = addTheseAttrs.concat(fakeAttrs);

              await exec();

              expect(relayer.selector).toEqual(customSelector);

              const customSelectorTarget = page.doc.querySelector(customSelector);
              expect(page.root.innerHTML).toBeTruthy(); // just to be sure it exists

              const expectedAttr = addThisAttr.name;
              expect(customSelectorTarget).toHaveAttribute(expectedAttr);

              // And it should not relay it to the default
              expect(relayTarget).not.toHaveAttribute(expectedAttr);
            });
          });

          describe(`dont-relay option`, () => {
            it(`should not relay an attr which is selected by a provided regex`, async () => {
              const dontRelayOption = `${defaultRelayer.relayOptionPrefix}${kebabize(defaultRelayer.relayOptions.dontRelay.name)}='${JSON.stringify(['dont'])}'`;
              const attrDontRelay = `attr-dont-relay`;
              const attrDoRelayName = `attr-do-relay`;
              const attrDoRelayValue = `the value`;

              // We have to do this manually, because dontRelayOption needs single quotes
              page = await newSpecPage({
                components: [TestingRelay],
                html: `<testing-relay ${attrDontRelay} ${attrDoRelayName}="${attrDoRelayValue}" ${dontRelayOption}></testing-relay>`,
              });
              await page.waitForChanges();
              relayer = page.rootInstance.relayer;
              relayTarget = page.doc.querySelector(`[data-testid="relay-target"]`);

              expect(relayTarget).not.toHaveAttribute(attrDontRelay);
              expect(relayTarget.getAttribute(attrDoRelayName)).toEqual(attrDoRelayValue);

              /** ALSO when it's added DYNAMICALLY */
              const dynAttrDontRelay = 'attr-also-dont-relay';
              const dynAttrDoRelay = 'attr-also-do-relay';
              page.root.setAttribute(dynAttrDontRelay, '');
              page.root.setAttribute(dynAttrDoRelay, '');

              // Because the MutationObserver doesn't work with Jest, call this directly
              relayer.onAttributesChange([dynAttrDontRelay, dynAttrDoRelay]);

              expect(relayTarget).not.toHaveAttribute(dynAttrDontRelay);
              expect(relayTarget).toHaveAttribute(dynAttrDoRelay);
            });
          });

          describe(`force-relay option`, () => {
            it(`should force relay an attr which is selected by a provided regex`, async () => {
              const forceRelayOption = `${defaultRelayer.relayOptionPrefix}${kebabize(defaultRelayer.relayOptions.forceRelay.name)}='${JSON.stringify(['this'])}'`;
              const attrForceRelayName = kebabizedAddedHostProp;
              const attrDoRelayValue = `the value`;

              // We have to do this manually, because dontRelayOption needs single quotes
              page = await newSpecPage({
                components: [TestingRelay],
                html: `<testing-relay ${attrForceRelayName}="${attrDoRelayValue}" ${forceRelayOption}></testing-relay>`,
              });
              await page.waitForChanges();
              relayer = page.rootInstance.relayer;
              relayTarget = page.doc.querySelector(`[data-testid="relay-target"]`);

              expect(relayTarget).toHaveAttribute(attrForceRelayName);
            });
            it(`should force relay, even when it's added to dont relay option`, async () => {
              const regex = 'ffforce';
              const dontRelayOption = `${defaultRelayer.relayOptionPrefix}${kebabize(defaultRelayer.relayOptions.dontRelay.name)}='${JSON.stringify([regex])}'`;
              const forceRelayOption = `${defaultRelayer.relayOptionPrefix}${kebabize(defaultRelayer.relayOptions.forceRelay.name)}='${JSON.stringify([regex])}'`;
              const attrForceRelayName = `test-${regex}-some-more`;
              const attrDoRelayValue = `the value`;

              // We have to do this manually, because dontRelayOption needs single quotes
              page = await newSpecPage({
                components: [TestingRelay],
                html: `<testing-relay ${attrForceRelayName}="${attrDoRelayValue}" ${dontRelayOption} ${forceRelayOption}></testing-relay>`,
              });
              await page.waitForChanges();
              relayer = page.rootInstance.relayer;
              relayTarget = page.doc.querySelector(`[data-testid="relay-target"]`);

              expect(relayTarget).toHaveAttribute(attrForceRelayName);
            });
          });

          describe(`wrong option`, () => {
            beforeEach(() => {
              console.error = jest.fn();
              addTheseAttrs.push({
                name: `${defaultRelayer.relayOptionPrefix}doesnt-exist-${Math.random()}`,
                value: `shouldn't matter`,
              });
            });

            it(`should throw an error`, async () => {
              await expect(exec()).rejects.toThrow();
            });

            it(`should throw a list of all possible options when a wrong option is given`, async () => {
              try {
                await exec();
              } catch (e) {
                for (const option of Object.keys(relayer.relayOptions)) {
                  expect(e.message).toContain(kebabize(option));
                }
              }
            });
          });
        });
      });

      describe(`calling default methods`, () => {
        let testingRelayComponent: TestingRelay;

        beforeEach(() => {
          globalThis.calledFromComponentSpy = jest.fn();
        });

        it(`should have called original load method`, async () => {
          expect(globalThis.calledFromComponentSpy).not.toHaveBeenCalled();

          await exec();

          expect(globalThis.calledFromComponentSpy).toHaveBeenCalledWith('componentDidLoad');
        });

        it(`should have called original destroy method`, async () => {
          await exec();

          testingRelayComponent = page.rootInstance;
          const spy = jest.spyOn(testingRelayComponent, 'disconnectedCallback');

          expect(spy).not.toHaveBeenCalled();
          page.body.querySelector('testing-relay').remove();

          expect(spy).toHaveBeenCalled();
        });
      });

      describe(`cleanup`, () => {
        it(`should clean up the mutation observer after element is removed`, async () => {
          await exec();

          const cleanupSpy = spyOn(relayer.mutationObserver, 'disconnect');
          page.body.querySelector('testing-relay').remove();

          expect(cleanupSpy).toHaveBeenCalled();
        });
      });
    });
  });
});

@Component({
  tag: 'testing-relay',
  scoped: true,
})
class TestingRelay {
  @Element() el: HTMLElement;
  @RelayAttrs() relayedAttrs: attrs;
  @Prop() thisOneIsAddedToTheHostProp;
  @Prop() nocamelcasehostprop;

  componentDidLoad() {
    this.callSpy('componentDidLoad');
  }

  disconnectedCallback() {
    this.callSpy('disconnectedCallback');
  }

  callSpy(str: string) {
    typeof globalThis.calledFromComponentSpy === 'function' && globalThis.calledFromComponentSpy(str);
  }

  render() {
    return (
      <div>
        <input data-testid="relay-target" {...this.relayedAttrs} />
        <span for-custom-selector></span>
      </div>
    );
  }
}

@Component({
  tag: 'testing-relay-shadowed',
  shadow: true,
})
class TestingRelayShadowed {
  @Element() el: HTMLElement;
  @RelayAttrs() relayedAttrs: attrs;

  render() {
    return (
      <div>
        <input data-testid="relay-target" {...this.relayedAttrs} />
      </div>
    );
  }
}
